//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: RunAction.cc  $
//
/// \file bioex/src/RunAction.cc
/// \brief Implementation of the RunAction class
//

#include "RunAction.hh"
#include "DetectorConstruction.hh"
#include "PrimaryGeneratorAction.hh"
#include "ParallelWorld.hh"
#include "Run.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "SteppingAction.hh"
#include <fstream>
#include "G4String.hh"

#ifdef G4MULTITHREADED
#include "G4Threading.hh"
#endif

#include "G4THitsMap.hh"

using namespace std;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
RunAction::RunAction()
:fpRun(0),fpDetector(0),fNx(0),fNy(0),fNz(0)
{
  fpDetector =
      dynamic_cast<const DetectorConstruction*>(G4RunManager::GetRunManager()
          ->GetUserDetectorConstruction());
  //
  // vector represents a list of MultiFunctionalDetector names
  fSDName.push_back(G4String("VoxDet"));

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
RunAction::~RunAction()
{
    fSDName.clear();
    fParticleInfo.clear();
}

G4Run* RunAction::GenerateRun()
{
  return new Run(fpDetector, fSDName);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void RunAction::BeginOfRunAction(const G4Run* aRun )
{
  G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void RunAction::EndOfRunAction(const G4Run* aRun)
{
  const DetectorConstruction* detector =
  (const DetectorConstruction*)
         (G4RunManager::GetRunManager()->GetUserDetectorConstruction());

  if ((!isMaster) && detector->IsSensCube()) {
     PrintParticleInfo((G4String ) "output");
   }
   if ((!isMaster) ) {
      return;
    }

   ParallelWorld* pW = (ParallelWorld*) detector->GetParallelWorld(0);
   pW->GetNumberOfSegmentsInPhantom(fNx,fNy,fNz);//Fill fNx,y,z.

  fpRun->EndOfRun();

  Run* theRun = (Run*)aRun;
  //theRun->DumpAllScorer();

  theRun->PrintDeposits();
  theRun->PrintCubeDeposits();

  G4THitsMap<G4double>* totEdep  = theRun->GetHitsMap("VoxDet/eDep");
  G4THitsMap<G4double>* dose  = theRun->GetHitsMap("VoxDet/dose");
  G4THitsMap<G4double>* trackL  = theRun->GetHitsMap("VoxDet/trackLength");

  G4cout << "============================================="<<G4endl;

  //G4double voxSize = pW->GetVoxelSize();

  std::ofstream  file(detector->GetOutputFileName());
  file << " 3D dep (eV) and dose (Gy) and trackLength (mm) distribution" << std::endl;
  file << " Voxel size = " << pW->GetVoxelSize()/mm << " mm. "<< std::endl;
  file << " Number of Vox in X,Y,Z direction (Y is vertical) "
       << fNx << " "
       << fNy << " "
       << fNz << " " << std::endl;

  // print it to the file
  for (G4int iz = 0; iz < fNz; iz++){
    for (G4int iy = 0; iy < fNy; iy++){
      for (G4int ix = 0; ix < fNx; ix++){
        G4double* totED = (*totEdep)[CopyNo(ix,iy,iz)];
        G4double* dos = (*dose)[CopyNo(ix,iy,iz)];
        G4double* tL = (*trackL)[CopyNo(ix,iy,iz)];

        G4bool empty1,empty2, empty3;
        empty1 = empty2 = empty3 = false;

        if ( !totED ) { totED = new G4double(0.0);  empty1 = true; }
        if (!dos) { dos = new G4double(0.0); empty2 = true; }
        if (!tL)  {tL = new G4double(0.0); empty3 = true;}

        file << ix << " "<< iy <<" "<< iz <<" "
        << *totED/eV << " " << *dos/gray << " "
        << *tL/um    << " " << std::endl;

        if ( empty1 ) { delete totED; empty1 = false; }
        if ( empty2 ) { delete dos; empty2 = false; }
        if ( !tL )    { delete tL; empty3 = false; }
      }
    }
  }
  file.close();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void RunAction::PrintParticleInfo(G4String filename)
{
  ofstream output;
  string ss = "";
  stringstream out;
  // for multithreaded add thread number to the file name
  #ifdef G4MULTITHREADED
      G4int threadID = G4Threading::G4GetThreadId();
      out << threadID;
    	ss = "_"+out.str();
  #endif

  output.open( filename+ss+".txt", ios::trunc );
  const G4int colw = 12;
  output << "Particle  Energy [eV]  PositionX PosY PosZ [mm] Momentum Direction X Y Z  time [ns]" << endl;

  for (std::vector<particleInfo>::iterator it=fParticleInfo.begin(); it!=fParticleInfo.end(); ++it){
      particleInfo part = (*it);
      G4ThreeVector pos = part.position;
      G4ThreeVector dir = part.momentumDir;
      output << setw(colw) << part.particleName << " "
      << setw(colw) << part.energy/eV  << " "
      << setw(colw) << pos.getX()/mm   << " "
      << setw(colw) << pos.getY()/mm   << " "
      << setw(colw) << pos.getZ()/mm   << " "
      << setw(colw) << dir.getX()      << " "
      << setw(colw) << dir.getY()      << " "
      << setw(colw) << dir.getZ()      << " "
      << setw(colw) << part.time/ns << endl;
  }
  output.close();
}
