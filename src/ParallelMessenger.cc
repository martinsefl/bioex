//
//   BioEx
/// \file bioex/src/ParallelMessenger.cc
/// \brief Implementation of the ParallelMessenger class
//
// $Id: ParallelMessenger.cc  $


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "ParallelMessenger.hh"
//#include "G4tgbVolumeMgr.hh"

#include "ParallelWorld.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4RunManager.hh"

ParallelMessenger::ParallelMessenger(ParallelWorld * W)
:G4UImessenger(),fParallel(W), fVoxDir(0),fVoxelSize(0),fNumberOfVoxels(0),
fPositionOfGrid(0)

{
  fVoxDir = new G4UIdirectory("/bioex/voxels/");
  fVoxDir->SetGuidance("Defining dimensions of the voxel grid.");

  fVoxelSize= new G4UIcmdWithADoubleAndUnit("/bioex/voxels/voxelSize",this);
  fVoxelSize->SetGuidance("Enter a size of a voxel.");
  fVoxelSize->SetGuidance("mm is a default unit.");
  fVoxelSize->SetParameterName("length",false);
  fVoxelSize->SetUnitCategory("Length");
  fVoxelSize->SetDefaultUnit("mm");
  fVoxelSize->AvailableForStates(G4State_PreInit);
  fVoxelSize->SetToBeBroadcasted(false);

  fNumberOfVoxels = new G4UIcmdWith3Vector("/bioex/voxels/numOfVoxelsXYZ",this);
  fNumberOfVoxels->SetGuidance("Set Number of Voxels along x,y,z axes.");
  fNumberOfVoxels->AvailableForStates(G4State_PreInit);
  fNumberOfVoxels->SetToBeBroadcasted(false);

  fPositionOfGrid = new G4UIcmdWith3VectorAndUnit("/bioex/voxels/positionOfGrid",this);
  fPositionOfGrid->SetGuidance("Set Position of the center of the grid x,y,z. Def. in mm.");
  fPositionOfGrid->SetUnitCategory("Length");
  fPositionOfGrid->SetDefaultUnit("mm");
  fPositionOfGrid->AvailableForStates(G4State_PreInit);
  fPositionOfGrid->SetToBeBroadcasted(false);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ParallelMessenger::~ParallelMessenger()
{
  delete fVoxDir;
  delete fVoxelSize;
  delete fNumberOfVoxels;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ParallelMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{

  if (command == fVoxelSize )
    {
      G4double voxSize = fVoxelSize->GetNewDoubleValue(newValue);
      fParallel->SetVoxelSize(voxSize);
    }

  if (command == fNumberOfVoxels )
    {
      G4ThreeVector dimensions = fNumberOfVoxels->GetNew3VectorValue(newValue);

      fParallel->SetNumberOfVoxels(dimensions);
    }
  if (command == fPositionOfGrid )
      {
        G4ThreeVector position = fNumberOfVoxels->GetNew3VectorValue(newValue);
        // already multiplied by a unit
        fParallel->SetPositionOfGrid(position);
      }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
