#include "Flask.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4Region.hh"
#include "G4ProductionCuts.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4VSolid.hh"
#include "G4SubtractionSolid.hh"

#include "G4RotationMatrix.hh"
#include "G4ExtrudedSolid.hh"
#include "G4NistManager.hh"
#include "G4Material.hh"
#include "G4PhysicalConstants.hh"
#include "G4Transform3D.hh"
#include "G4PVPlacement.hh"

#include "G4TwoVector.hh"
// $Id: Flask.cc  $
//
/// \file bioex/src/Flask.cc
/// \brief Implementation of the Flask class, TPP Flask 25


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
Flask::Flask() : TestTube()
{
  R00 = G4RotationMatrix();
  RU0 = G4RotationMatrix(); RU0.rotateY(pi);
  R01 = G4RotationMatrix(); R01.rotateX(-halfpi);

    // bottom
    hW0 = 47.8*mm;
    hL0 = 45.0*mm;
    hNh0 = 61.2*mm-hL0;
    hNw0 = 19.6*mm;
    hH = 24.8*mm/2.;
    wW = 1*mm;

    pointsOut.push_back(G4TwoVector(-hL0/2.,-hW0/2.));
    pointsOut.push_back(G4TwoVector( hL0/2.,-hW0/2.));
    pointsOut.push_back(G4TwoVector( hL0/2.+hNh0,-hNw0/2.));
    pointsOut.push_back(G4TwoVector( hL0/2.+hNh0, hNw0/2.));
    pointsOut.push_back(G4TwoVector( hL0/2., hW0/2.));
    pointsOut.push_back(G4TwoVector(-hL0/2., hW0/2.));

    pointsIn.push_back(G4TwoVector(-hL0/2.+wW,-hW0/2.+wW));
    pointsIn.push_back(G4TwoVector( hL0/2.-wW,-hW0/2.+wW));
    pointsIn.push_back(G4TwoVector( hL0/2.+hNh0-wW,-hNw0/2.+wW));
    pointsIn.push_back(G4TwoVector( hL0/2.+hNh0-wW, hNw0/2.-wW));
    pointsIn.push_back(G4TwoVector( hL0/2.-wW, hW0/2.-wW));
    pointsIn.push_back(G4TwoVector(-hL0/2.+wW, hW0/2.-wW));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
Flask::~Flask()
{ ; }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4VPhysicalVolume* Flask::ConstructTestTube(G4VPhysicalVolume* motherVolume)
{

   G4NistManager* man = G4NistManager::Instance();
   man->SetVerbose(1);
   G4Material* tube_mat = man->FindOrBuildMaterial("G4_POLYPROPYLENE");




   G4VSolid * flaskSolidOut = new G4ExtrudedSolid("flaskOut",
       pointsOut, hH,  G4TwoVector(0, 0), 1.0, G4TwoVector(0, 0), 1.0);
   G4VSolid * flaskSolidIn = new G4ExtrudedSolid("flaskIn",
       pointsIn, hH-wW,  G4TwoVector(0, 0), 1.0, G4TwoVector(0, 0), 1.0);
   G4VSolid * flaskSolid = new G4SubtractionSolid("flask",
            flaskSolidOut, flaskSolidIn);


   G4LogicalVolume * flaskLogic = new G4LogicalVolume(flaskSolid,
              tube_mat,"flask", 0,0,0,true);
   G4Transform3D place1 (R01,G4ThreeVector(0,0,0));
   G4VPhysicalVolume* flaskPhy = new G4PVPlacement(place1,"flask",
             flaskLogic, motherVolume, false, 0, true);

   return flaskPhy;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4VPhysicalVolume* Flask::ConstructSample(
  G4VPhysicalVolume* motherVolume, G4double volumeOfSample = 1*cm3)
{
  G4NistManager* man = G4NistManager::Instance();
  man->SetVerbose(1);
  G4Material* sample_mat = man->FindOrBuildMaterial("G4_WATER");

  std::vector <G4TwoVector> points;

  G4LogicalVolume * sampleLogic = 0;
  G4VPhysicalVolume * samplePhy = 0;
  G4VSolid * sampleSolid = 0;

  G4double area1 = (hL0-wW)*(hW0-wW);
  G4double area2 = ((hW0+hNw0)/2.-wW)*(hNh0-wW);
  G4double area = area1+area2;
  std::cout << "AREA " << area/(cm2) << " cm2" << std::endl;
  G4double sam_width =volumeOfSample/area;

  sampleSolid = new G4ExtrudedSolid("sample",
      pointsIn, sam_width/2.,  G4TwoVector(0, 0), 1.0, G4TwoVector(0, 0), 1.0
  );
  sampleLogic = new G4LogicalVolume(sampleSolid,
             sample_mat,"sample", 0,0,0,true);
  G4Transform3D place1 (R01,G4ThreeVector(0,0-hH+wW+sam_width/2.,0));
  samplePhy = new G4PVPlacement(place1,"sample",
            sampleLogic, motherVolume, false, 0, true);


  G4Region * fpRegion = new G4Region("Target");
  fpRegion->AddRootLogicalVolume(sampleLogic);


  return samplePhy;
}
