//
//   BioEx
/// \file bioex/src/DetectorMessenger.cc
/// \brief Implementation of the DetectorMessenger class
//
// $Id: DetectorMessenger.cc  $


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "DetectorMessenger.hh"
//#include "G4tgbVolumeMgr.hh"

#include "DetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4RunManager.hh"

DetectorMessenger::DetectorMessenger(DetectorConstruction * Det)
:G4UImessenger(),fDetector(Det), fBioExDir(0), fDetDir(0),
fFileNameCmd(0),fSampleVolumeCmd(0)
{
  fBioExDir = new G4UIdirectory("/bioex/");
  fBioExDir->SetGuidance("commands specific to bioex app");

  fDetDir = new G4UIdirectory("/bioex/det/");
  fDetDir->SetGuidance("handling of geometry");

//  fTestTubeDir = new G4UIdirectory("/bioex/testTube");
//  fTestTubeDir->SetGuidance("Choose the Test Tube");

  fFileNameCmd = new G4UIcmdWithAString("/bioex/det/FileName",this);
  fFileNameCmd->SetGuidance("Provide the filename with geometry.");
  fFileNameCmd->SetParameterName("choice",false);
  fFileNameCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  fFileNameCmd->SetToBeBroadcasted(false);

  fConstructCmd = new G4UIcmdWithoutParameter("/bioex/det/construct",this);
  fConstructCmd->SetGuidance("Construct geometry.");
  fConstructCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  fConstructCmd->SetToBeBroadcasted(false);

  fSampleVolumeCmd = new G4UIcmdWithADoubleAndUnit("/bioex/det/sampleVolume",this);
  fSampleVolumeCmd->SetGuidance("Enter the volume of sample in a test tube.");
  fSampleVolumeCmd->SetGuidance("cm3 is a default unit.");
  fSampleVolumeCmd->SetParameterName("volume",false);
  fSampleVolumeCmd->SetUnitCategory("Volume");
  fSampleVolumeCmd->SetDefaultUnit("cm3");
  fSampleVolumeCmd->AvailableForStates(G4State_PreInit);
  fSampleVolumeCmd->SetToBeBroadcasted(false);

  fTestTubeChoice = new G4UIcmdWithAString("/bioex/testTube",this);

  fOutputFileNameCmd = new G4UIcmdWithAString("/bioex/det/outputFileName",this);
  fOutputFileNameCmd->SetGuidance("Provide the name for the output file.");
  fOutputFileNameCmd->SetParameterName("choice",false);
  fOutputFileNameCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  fOutputFileNameCmd->SetToBeBroadcasted(false);


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorMessenger::~DetectorMessenger()
{
  delete fFileNameCmd;
  delete fDetDir;
  delete fBioExDir;
  //delete fTestTubeDir;
  delete fConstructCmd;
  delete fSampleVolumeCmd;
  delete fTestTubeChoice;
  delete fOutputFileNameCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
   if( command == fFileNameCmd )
    {  fDetector->SetFileName(newValue); }

  if (command == fConstructCmd )
    {  fDetector->Construct();
      G4RunManager::GetRunManager()->ReinitializeGeometry();
    }

  if (command == fSampleVolumeCmd )
    {
      G4double volSam = fSampleVolumeCmd->GetNewDoubleValue(newValue);
      fDetector->SetSampleVolume(volSam);
    }

  if (command == fTestTubeChoice )
    {
      fDetector->SetTestTube(newValue);
    }

  if (command == fOutputFileNameCmd )
    {
      fDetector->SetOutputFileName(newValue);
    }



}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
