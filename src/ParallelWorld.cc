/// \file bioex/src/ParallelWorld.cc
/// \brief Implementation of the ParallelWorld class
//
// $Id: ParallelWorld.cc  $
#include "ParallelWorld.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4Box.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4PVReplica.hh"
#include "ParallelMessenger.hh"
#include "G4SDManager.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSEnergyDeposit3D.hh"
#include "PSEnergyDeposit.hh"
#include "PSDoseDeposit3D.hh"
#include "G4PSTrackLength.hh"
#include "Run.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ParallelWorld::ParallelWorld(G4String worldName)
:G4VUserParallelWorld(worldName),ghLogical(0),
yRepLog(0),xRepLog(0),zRepLog(0)
{
  voxSize = 1*mm;
  fNX = 10; fNY = 20; fNZ = 10;
  fPX = 0.; fPY = 0.; fPZ = 0.;
  fParallelMessenger = new ParallelMessenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ParallelWorld::~ParallelWorld()
{
  delete fParallelMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


void ParallelWorld::Construct()
{
  G4VPhysicalVolume* ghostWorld = GetWorld();
  G4LogicalVolume* worldLogical = ghostWorld->GetLogicalVolume();

  // place volumes in the parallel world here.
  //
  G4Box * pWSolid =
  new G4Box("ParallelBox", fNX*voxSize/2., fNY*voxSize/2., fNZ*voxSize/2.);
  ghLogical = new G4LogicalVolume(pWSolid, 0, "ParallelLogical", 0, 0, 0);
  new G4PVPlacement(0, G4ThreeVector(fPX,fPY,fPZ), ghLogical,
                    "ParallelPhysical", worldLogical, 0, 0);

  // voxels, and their assemblies
  G4String yRepName("yRep");
  G4String xRepName("xRep");
  G4String zRepName("zRep");
  G4double voxHL = voxSize/2.;  // voxel half length

  G4Box * yRepSol = new G4Box(yRepName, fNX*voxHL, voxHL, fNZ*voxHL);
  G4Box * xRepSol = new G4Box(xRepName, voxHL, voxHL, fNZ*voxHL);
  G4Box * zRepSol = new G4Box(zRepName, voxHL, voxHL, voxHL);

  yRepLog = new G4LogicalVolume(yRepSol, 0, yRepName, 0, 0, 0);
  xRepLog = new G4LogicalVolume(xRepSol, 0, xRepName, 0, 0, 0);
  zRepLog = new G4LogicalVolume(zRepSol, 0, zRepName, 0, 0, 0);

  new G4PVReplica	(	yRepName, yRepLog, ghLogical, kYAxis, fNY, voxSize);
  new G4PVReplica ( xRepName, xRepLog, yRepLog, kXAxis, fNX,voxSize);
  new G4PVReplica ( zRepName, zRepLog, xRepLog, kZAxis, fNZ, voxSize);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ParallelWorld::ConstructSD()
{
    SetupDetectors();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ParallelWorld::SetNumberOfVoxels(G4ThreeVector nums)
{
   fNX = (G4int) nums.getX();
   fNY = (G4int) nums.getY();
   fNZ = (G4int) nums.getZ();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ParallelWorld::SetPositionOfGrid(G4ThreeVector pos)
{
   fPX = (G4int) pos.getX();
   fPY = (G4int) pos.getY();
   fPZ = (G4int) pos.getZ();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ParallelWorld::SetupDetectors()
{
  G4SDManager* pSDMan = G4SDManager::GetSDMpointer();
  pSDMan->SetVerboseLevel(1);
  // sensitive detector name
  G4String detName = "VoxDet";

  // define MultiFunctionalDetector with the name.
  G4MultiFunctionalDetector* mFDet = new G4MultiFunctionalDetector(detName);
  // register detector to SDManager
  pSDMan->AddNewDetector(mFDet);
  // Assign SD to the logical Volume
  zRepLog->SetSensitiveDetector(mFDet);

  // Particle filters could be defined here. We do not care now.


  // Primitive Scorers
  //
  G4VPrimitiveScorer* primitive;
  primitive = new PSEnergyDeposit("eDep",fNX,fNY,fNZ);
  mFDet->RegisterPrimitive(primitive);

  primitive = new PSDoseDeposit3D("dose",fNX,fNY,fNZ);
  mFDet->RegisterPrimitive(primitive);

  primitive = new G4PSTrackLength("trackLength");
  mFDet->RegisterPrimitive(primitive);

  pSDMan->SetVerboseLevel(0);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
