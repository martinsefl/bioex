#include "Eppe_nph.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4Region.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4VSolid.hh"
#include "G4Sphere.hh"
#include "G4Cons.hh"
#include "G4RotationMatrix.hh"
#include "G4UnionSolid.hh"
#include "G4NistManager.hh"
#include "G4Material.hh"
#include "G4PhysicalConstants.hh"
#include "G4Transform3D.hh"
#include "G4PVPlacement.hh"

#include "G4Tubs.hh"
#include "G4IntersectionSolid.hh"

//
//     |         |   fInnerUpperRad fOuterUpperRad
//     |         |
//     |         |
//     |         |
//     \         |   fInnerMidRad fOuterMidRad
//      \       /
//       \     /
//        |   /      fInnerTipRad fOuterTipRad
//         (_)
//
// $Id: Eppe_nph.cc  $
//
/// \file bioex/src/Eppe_nph.cc
/// \brief Implementation of the Eppe_nph class, Eppendorf 1.5 ml


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
Eppe_nph::Eppe_nph() : TestTube()
{
    fInnerUpperRad = 4.0*mm/2.;    fOuterUpperRad = 5.6*mm/2.; // inner/outer upper radius of upper cone
    fInnerMidRad = 4.0*mm/2.;    fInnerTipRad = 2.3*mm/2.;
    // inner bottom radius of upper cone/upper inner
    // radius of midcone, and radius of half sphere
    fOuterTipRad = 3.9*mm/2.;    fOuterMidRad = 5.6*mm/2.;
    // outer radius of halfsphere and outer upper radius of middle cone
    fUpperHeight = 28.48*mm/2.;    fBottomHeight = 17.57*mm/2.;
    // half-heights of cones

    R00 = G4RotationMatrix();
    RU0 = G4RotationMatrix(); RU0.rotateY(pi);
    R01 = G4RotationMatrix(); R01.rotateX(-halfpi);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
Eppe_nph::~Eppe_nph()
{ ; }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4VPhysicalVolume* Eppe_nph::ConstructTestTube(G4VPhysicalVolume* motherVolume)
{

   G4NistManager* man = G4NistManager::Instance();
   man->SetVerbose(1);
   G4Material* tube_mat = man->FindOrBuildMaterial("G4_POLYPROPYLENE");

   G4VSolid * Eppe_nphsph = new G4Sphere("Eppe_nphsph",fInnerTipRad,fOuterTipRad,0.,twopi,0.,halfpi);
   G4VSolid * Eppe_nphcon = new G4Cons("Eppe_nphcon",fInnerTipRad,fOuterTipRad,fInnerMidRad,fOuterMidRad,fBottomHeight,0.,twopi);
   G4VSolid * Eppe_nphcon2= new G4Cons("Eppe_nphcon2",fInnerMidRad,fOuterMidRad,fInnerUpperRad,fOuterUpperRad,fUpperHeight,0.,twopi);

   G4ThreeVector ztrans1(0.,0.,fUpperHeight+fBottomHeight);
   G4ThreeVector ztrans0(0.,0., -fBottomHeight);
   G4Transform3D trans0(RU0, ztrans0);
   G4Transform3D trans1(R00, ztrans1);

   G4VSolid * Eppe_nphhelp = new G4UnionSolid("Eppe_nphhelp",
              Eppe_nphcon, Eppe_nphsph, trans0);
   G4VSolid * Eppe_nphsolid = new G4UnionSolid("Eppe_nphhelp2",
              Eppe_nphhelp, Eppe_nphcon2, trans1);

   G4LogicalVolume * Eppe_nphlogic = new G4LogicalVolume(Eppe_nphsolid,
              tube_mat,"Eppe_nph", 0,0,0,true);
   G4Transform3D place1 (R01,G4ThreeVector(0,0,0));
   G4VPhysicalVolume* Eppe_nphphy = new G4PVPlacement(place1,"Eppe_nph",
              Eppe_nphlogic, motherVolume, false, 0, true);

   return Eppe_nphphy;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4VPhysicalVolume* Eppe_nph::ConstructSample(
  G4VPhysicalVolume* motherVolume, G4double volumeOfSample = 1*cm3)
{
  G4NistManager* man = G4NistManager::Instance();
  man->SetVerbose(1);
  G4Material* sample_mat = man->FindOrBuildMaterial("G4_WATER");

  G4double sphVol = 2*fInnerTipRad*fInnerTipRad*fInnerTipRad*pi/3 ;
  G4double cone1Vol = 2*pi*fBottomHeight*(fInnerTipRad*fInnerTipRad + fInnerMidRad*fInnerTipRad + fInnerMidRad*fInnerMidRad)/3.;
  G4double cone2Vol = 2*pi*fUpperHeight*(fInnerUpperRad*fInnerUpperRad + fInnerMidRad*fInnerUpperRad + fInnerMidRad*fInnerMidRad)/3.;

  G4LogicalVolume * sampleLog;
  G4VPhysicalVolume * samplePhy;
  G4VSolid * sampleSph;

  if (volumeOfSample <= sphVol){

      sampleSph = new G4Sphere("sampleSph", 0., fInnerTipRad, 0., twopi, 0.,halfpi);
      G4double height = GetSphereHeight(volumeOfSample,fInnerTipRad);

      G4Tubs* cylinder =  new G4Tubs("Cylinder",0,10.*mm,height,0.,twopi);
      G4ThreeVector  zT(0.,0.,fInnerTipRad);
      G4Transform3D  transform(R00,zT);
      G4VSolid * sampleHelp  = new G4IntersectionSolid("sample",
            sampleSph, cylinder, transform);
      sampleLog = new G4LogicalVolume(sampleHelp,sample_mat,
            "sample", 0,0,0,true);

      G4RotationMatrix R02= G4RotationMatrix();
      R02.rotateX(halfpi);//,0,0);
      G4ThreeVector ztrans4(0.,-fBottomHeight, 0.);
      // it has to be moved from the origin to bottom of the tube -fBottomHeight
      G4Transform3D trans4(R02, ztrans4);
      samplePhy = new G4PVPlacement(trans4, "sample",sampleLog,
            motherVolume, false, 0, true);

  } else if ( volumeOfSample <= ( sphVol + cone1Vol ) ) {

      sampleSph = new G4Sphere("sampleSph", 0., fInnerTipRad, 0., twopi, 0., halfpi);
      G4double restVol = volumeOfSample - sphVol;
      G4double height = GetConeHeight(fInnerMidRad,fInnerTipRad,fBottomHeight,restVol)/2.;
      G4double in_rad1 = GetRadiusOfConePart(fInnerMidRad,fInnerTipRad,fBottomHeight,height);
      // inner radius has to be changing with the height
      G4VSolid * sampleCone1 = new G4Cons("sampleCone1",
            0., fInnerTipRad, 0., in_rad1, height, 0., twopi);

      G4ThreeVector ztrans0(0.,0., -height);
      G4Transform3D trans0(RU0, ztrans0);

      G4VSolid * sampleHelp = new G4UnionSolid("sample",
            sampleCone1, sampleSph,trans0);
      std::cout << "Union vol: " << sampleHelp->GetCubicVolume()/cm3 <<
                                    " cm3." << std::endl;
      std::cout << "Half sphere vol: " << sampleSph->GetCubicVolume()/cm3 <<
                                    " cm3." << std::endl;
      std::cout << "Cone 1 vol: " << sampleCone1->GetCubicVolume()/cm3 <<
                                    " cm3." << std::endl;
      sampleLog = new G4LogicalVolume(sampleHelp,sample_mat,"sample",
            0,0,0,true);

      G4ThreeVector ztrans4(0.,-fBottomHeight+height,0);
      G4Transform3D trans4(R01, ztrans4);
      samplePhy = new G4PVPlacement(trans4, "sample",sampleLog,
            motherVolume, false, 0, true);

  } else if ( volumeOfSample <= (sphVol + cone1Vol+ cone2Vol)) {

      sampleSph = new G4Sphere("sampleSph", 0., fInnerTipRad, 0., twopi, 0.,halfpi);
      G4double restVol = volumeOfSample - sphVol - cone1Vol;
      G4double height = GetConeHeight(fInnerUpperRad,fInnerMidRad,fUpperHeight,restVol)/2.;
      G4double in_rad1 = GetRadiusOfConePart(fInnerUpperRad,fInnerMidRad,fUpperHeight,height);
      G4ThreeVector ztrans0(0.,0., -fBottomHeight);
      G4Transform3D trans0(RU0, ztrans0);
      G4ThreeVector ztrans2(0.,0.,fBottomHeight+height);
      G4Transform3D trans2 (R00,ztrans2);
      G4VSolid * sampleCone1 = new G4Cons("sampleCone1",
                 0.,fInnerTipRad,0.,fInnerMidRad,fBottomHeight, 0., twopi);
      G4VSolid * sampleCone2 = new G4Cons("sampleCone2",
                 0.,fInnerMidRad,0.,in_rad1, height,0., twopi);
      G4VSolid * sampleHelp = new G4UnionSolid("sampleHelp",
             sampleCone1, sampleSph, trans0);
      G4VSolid * sample = new G4UnionSolid("sample",
             sampleHelp, sampleCone2, trans2);
      sampleLog = new G4LogicalVolume(sample,sample_mat, "sample", 0,0,0,true);

      G4ThreeVector ztrans4(0.,0.,0);
      G4Transform3D trans4(R01, ztrans4);
      samplePhy = new G4PVPlacement(trans4, "sample",sampleLog,
            motherVolume, false, 0, true);


  } else {

      G4cout << "Too large sample volume." << G4endl;
      sampleSph = new G4Sphere("sampleSph", 0., fInnerTipRad, 0. , twopi, 0.,halfpi);
      G4ThreeVector ztrans0(0.,0., -fBottomHeight);
      G4Transform3D trans0(RU0, ztrans0);
      G4ThreeVector ztrans3(0.,0.,fBottomHeight+fUpperHeight);
      G4Transform3D trans3(R00,ztrans3);

      G4VSolid * sampleCone1 = new G4Cons("sampleCone1", 0., fInnerTipRad,
             0. , fInnerMidRad, fBottomHeight,0., twopi);
      G4VSolid * sampleCone2 = new G4Cons("sampleCone2", 0., fInnerMidRad,
             0. , fInnerUpperRad, fUpperHeight,0., twopi);
      G4VSolid * sampleHelp = new G4UnionSolid("sampleHelp",
             sampleCone1, sampleSph,trans0);
      G4VSolid * sample = new G4UnionSolid("sample",
             sampleHelp, sampleCone2, trans3);
      sampleLog = new G4LogicalVolume(sample,sample_mat, "sample", 0,0,0,true);

      // placement
      G4ThreeVector ztrans4(0.,0.,0);
      G4Transform3D trans4(R01, ztrans4);
      samplePhy = new G4PVPlacement(trans4, "sample",sampleLog,
            motherVolume, false, 0, true);
  }

  G4Region * fpRegion = new G4Region("Target");
  fpRegion->AddRootLogicalVolume(sampleLog);


  return samplePhy;
}
