//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// This example is provided by the Geant4-DNA collaboration
// Any report or published results obtained using the Geant4-DNA software
// shall cite the following Geant4-DNA collaboration publication:
// Med. Phys. 37 (2010) 4692-4708
// The Geant4-DNA web site is available at http://geant4-dna.org
//
// $ID SteppingAction.cc  $
/// \file bioex/src/SteppingAction.cc
/// \brief Implementation of the SteppingAction class


#include "SteppingAction.hh"
#include "RunAction.hh"
#include "DetectorConstruction.hh"
#include "G4RunManager.hh"

#include "G4SystemOfUnits.hh"
#include "G4SteppingManager.hh"
#include "G4VTouchable.hh"
#include "G4VPhysicalVolume.hh"
#include "G4Step.hh"
#include "G4VTouchable.hh"
#include "Run.hh"
#include "G4VPhysicalVolume.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction() : G4UserSteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* st)
{

    G4double edep  = st->GetTotalEnergyDeposit();
    if (edep>0.) {

      Run* cRun = (Run*)
            G4RunManager::GetRunManager()->GetCurrentRun();

    //  const G4StepPoint* preStepPoint = st->GetPreStepPoint();
    //  const G4StepPoint* postStepPoint = st->GetPostStepPoint();
    //  G4ThreeVector pos = (postStepPoint->GetPosition() -
    //         preStepPoint->GetPosition() ) * G4UniformRand();
    DetectorConstruction* det = (DetectorConstruction*)
    G4RunManager::GetRunManager()->GetUserDetectorConstruction();


//   The question is, if this always returns a volume from the mass world.
//   Could it give a volume from ParallelWorld?
/*     cout <<
     "Current Volume: "
     << st->GetPreStepPoint()->GetTouchableHandle()->GetVolume()->GetName()
     << endl;
     */

      G4VPhysicalVolume * sampleVol = det->GetSampleVolume();
      G4VPhysicalVolume * currvol =
           st->GetPreStepPoint()->GetTouchableHandle()->GetVolume();

      cRun->AddCubeDeposit(currvol->GetName(),edep);
      if ( sampleVol != currvol) return;
      cRun->AddEdep(edep);
  }

  const DetectorConstruction* detector =
  (const DetectorConstruction*)
         (G4RunManager::GetRunManager()->GetUserDetectorConstruction());
  if (detector)
  {
    RecordParticles(st);
  } // for particles coming, record their properties
}


void SteppingAction::RecordParticles(const G4Step* step)
{
  const DetectorConstruction* detector =
  (const DetectorConstruction*)
         (G4RunManager::GetRunManager()->GetUserDetectorConstruction());

  if (detector->IsSensCube()){

    DetectorConstruction* det = (DetectorConstruction*)
    G4RunManager::GetRunManager()->GetUserDetectorConstruction();

    G4VPhysicalVolume * sensitiveVol = det->GetSensitiveCube();

    if (sensitiveVol != step->GetPreStepPoint()->GetTouchableHandle()->GetVolume()) return;

    particleInfo part;

    const G4StepPoint* preStepPoint = step->GetPreStepPoint();

    if (preStepPoint->GetStepStatus() == fGeomBoundary){ // check if it has just entered

        part.particleName =
            step->GetTrack()->GetParticleDefinition()->GetParticleName();
        part.position = preStepPoint->GetPosition();
        part.energy = preStepPoint->GetKineticEnergy();
        part.momentumDir = preStepPoint->GetMomentumDirection();
        part.time = preStepPoint->GetGlobalTime();
        RunAction* ra = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
            ra->StoreParticleInfo(part);

    }
  }
}
