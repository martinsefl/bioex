//
// $Id: TestTube.cc  $
//
/// \file bioex/src/TestTube.cc
/// \brief Implementation of the TestTube class
/// it is a mother class from which other classes inherit

#include "TestTube.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "globals.hh"
#include "G4PhysicalConstants.hh"

// constructor
TestTube::TestTube()
{
;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
TestTube::~TestTube()
{ ; }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4double TestTube::GetConeHeight(G4double rad1, G4double rad2,
  G4double HH, G4double volume)
{
   G4double vol = volume/pi; // divided with pi to have easier calculation
   if (rad1 < rad2) {G4double rad3 = rad1; rad1 = rad2; rad2 = rad3; }
   // to ensure rad1 > rad2
  G4double H = HH*2.; // HH is half height

  G4double h1,h2, h;
  G4double vol1, tolerance;
  tolerance = 1.E-9*cm3;
  h1 = 0.;    // lower guess
  h2 = H;     // upper guess
  G4double r; // radius will be calculated for the given height
  do {

        h = (h2+h1)*0.5;
        r = h*(rad1 - rad2)/H + rad2;         // radius
        vol1 = h*(rad2*rad2+rad2*r+r*r)/3.;   // volume of the cone /pi
        if (vol1>vol) { h2 = h; } else { h1 = h; }

  } while ( (vol1 > (vol + tolerance)) || (vol1 < (vol - tolerance)) );

  return h;  // we need half height, this gives height
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double TestTube::GetRadiusOfConePart(G4double rad1, G4double rad2,
  G4double HH, G4double hei)
{
  if (rad1 < rad2) {G4double rad3 = rad1; rad1 = rad2; rad2 = rad3; }
  G4double r = hei*(rad1 - rad2)/HH + rad2;
  return r;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double TestTube::GetHalfEllipsoidHeight(G4double volume,G4double aX,
  G4double aY, G4double aZ)
{
  G4double vol = volume/pi; // divided with pi to have easier calculation

  G4double h1,h2, h;
  G4double vol1, tolerance;
  tolerance = 1.E-09*cm3;
  h1 = 0.;                  // lower guess
  h2 = aZ;              // upper guess

  do {

       h = (h2+h1)*0.5;
       vol1 = aX*aY*h*h*(3*aZ-h)/(3*aZ*aZ);

       if (vol1>vol) { h2 = h; } else { h1 = h; }

  } while ( (vol1 > (vol + tolerance)) || (vol1 < (vol - tolerance)) );

  return h;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4double TestTube::GetSphereHeight(G4double volume, G4double radius)
{
   G4double vol = volume/pi; // divided with pi to have easier calculation

   G4double h1,h2, h;
   G4double vol1, tolerance;
   tolerance = 1.E-09*cm3;
   h1 = 0.;                  // lower guess
   h2 = radius;              // upper guess

   do {

        h = (h2+h1)*0.5;
        vol1 = h*(6*radius*h-2*h*h)/6.;

        if (vol1>vol) { h2 = h; } else { h1 = h; }

   } while ( (vol1 > (vol + tolerance)) || (vol1 < (vol - tolerance)) );

   return h;
}
