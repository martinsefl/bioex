#include "Eppe20.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4Region.hh"
#include "G4ProductionCuts.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4VSolid.hh"
#include "G4Ellipsoid.hh"
#include "G4Cons.hh"
#include "G4RotationMatrix.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4NistManager.hh"
#include "G4Material.hh"
#include "G4PhysicalConstants.hh"
#include "G4Transform3D.hh"
#include "G4PVPlacement.hh"

#include "G4Tubs.hh"
#include "G4IntersectionSolid.hh"

//
//     |         |   IR0 OR0
//     |         |
//     |         |
//     |         |
//     \         |   IR1 OR1
//      \       /
//       \     /
//        _____
//
// $Id: Eppe20.cc  $
//
/// \file bioex/src/Eppe20.cc
/// \brief Implementation of the Eppe20 class, Eppendorf 2.0 ml

Eppe20::Eppe20() : TestTube()
{
    IR1 = 4.6*mm;   IR2 = 4.4*mm;
    // inner upper cone radius
    // bottom radius of cone, and 2 semiaxes of half ellipsoid
    IES = 6.7*mm; // inner ellips. semi axis perpendicular to IR2
    OES = 7.2*mm; // outer ellips. semiaxis

    OR2 = 5.3*mm;    OR1 = 5.4*mm;
    // outer radius of halfsphere and outer upper radius of middle cone
    CH1 = 16.15*mm;
    // half-heights of cones and water in upper cone

    R00 = G4RotationMatrix();
    RU0 = G4RotationMatrix(); RU0.rotateY(pi);
    R01 = G4RotationMatrix();  R01.rotateX(-halfpi);
    eppe20in = new G4Ellipsoid("eppe20in",IR2,IR2,IES,-0.1,IES);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
Eppe20::~Eppe20()
{ ; }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4VPhysicalVolume* Eppe20::ConstructTestTube(G4VPhysicalVolume* motherVolume)
{

   G4NistManager* man = G4NistManager::Instance();
   man->SetVerbose(1);
   G4Material* tube_mat = man->FindOrBuildMaterial("G4_POLYPROPYLENE");

   G4VSolid * eppe20out =
      new G4Ellipsoid("eppe20out",OR2,OR2,OES,0,OES);
   G4VSolid * eppe20tip =
      new G4SubtractionSolid("eppe20tip", eppe20out,eppe20in);

   G4VSolid * eppe20con = new G4Cons("eppe20con",IR2,OR2,IR1,OR1,CH1,0.,twopi);

   G4ThreeVector ztrans0(0.,0., -CH1);
   G4Transform3D trans0(RU0, ztrans0);

   G4VSolid * eppe20solid = new G4UnionSolid("tube",
              eppe20con,eppe20tip, trans0);

   G4LogicalVolume * eppe20logic = new G4LogicalVolume(eppe20solid,
              tube_mat,"tube", 0,0,0,true);
   G4Transform3D place1 (R01,G4ThreeVector(0,0,0));
   G4VPhysicalVolume* eppe20phy = new G4PVPlacement(place1,"tube",
              eppe20logic, motherVolume, false, 0, true);

   return eppe20phy;
}



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4VPhysicalVolume* Eppe20::ConstructSample(
  G4VPhysicalVolume* motherVolume, G4double volumeOfSample = 1.*cm3)
{
  G4NistManager* man = G4NistManager::Instance();
  man->SetVerbose(1);
  G4Material* sample_mat = man->FindOrBuildMaterial("G4_WATER");

  G4double ellVol = 2*IR2*IR2*IES*pi/3 ;
  //G4double cone1Vol = 2*pi*CH1*(IR2*IR2 + IR1*IR2 + IR1*IR1)/3.;

  G4LogicalVolume * sampleLog;
  G4VPhysicalVolume * samplePhy;
  G4VSolid * sampleEll;

  if (volumeOfSample <= ellVol){

      G4double height = GetHalfEllipsoidHeight(volumeOfSample,IR2,IR2,IES);
      sampleEll = new G4Ellipsoid("eppe20in",IR2,IR2,IES,IES-height,IES);
      sampleLog = new G4LogicalVolume(sampleEll,sample_mat,
            "sample", 0,0,0,true);

      G4ThreeVector vec1 = G4ThreeVector(0,-CH1,0);
      G4RotationMatrix rotZ90 = G4RotationMatrix();
      rotZ90.rotateX(halfpi);
      G4Transform3D trans0(rotZ90, vec1);

      samplePhy = new G4PVPlacement(trans0, "sample",sampleLog,
            motherVolume, false, 0, true);

  } else {

      if ( volumeOfSample > ( 2.0001*cm3 ) ) {
        G4cout << "Too large volume of sample chosen." << G4endl;
        G4cout << "Sample volume was set to 2.0 ml." << G4endl;
        volumeOfSample = 2.0*cm3;
      }
      sampleEll = new G4Ellipsoid("eppe20in",IR2,IR2,IES,0,IES);
      G4double restVol = volumeOfSample - ellVol;
      G4double height = GetConeHeight(IR1,IR2,CH1,restVol)/2.;
      G4double in_rad1 = GetRadiusOfConePart(IR1,IR2,CH1,height);

      G4VSolid * sampleCone1 = new G4Cons("sampleCone1",
            0., IR2, 0., in_rad1, height, 0., twopi);

      G4ThreeVector ztrans0(0.,0., -height);
      G4Transform3D trans0(RU0, ztrans0);

      G4VSolid * sampleHelp = new G4UnionSolid("sample",
            sampleCone1, sampleEll,trans0);
      std::cout << "Union vol: " << sampleHelp->GetCubicVolume()/cm3 <<
                                    " cm3." << std::endl;
      std::cout << "Half ellipsoid vol: " << sampleEll->GetCubicVolume()/cm3 <<
                                    " cm3." << std::endl;
      std::cout << "Cone 1 vol: " << sampleCone1->GetCubicVolume()/cm3 <<
                                    " cm3." << std::endl;
      sampleLog = new G4LogicalVolume(sampleHelp,sample_mat,"sample",
            0,0,0,true);

      G4ThreeVector ztrans4(0.,-CH1+height,0);
      G4Transform3D trans4(R01, ztrans4);
      samplePhy = new G4PVPlacement(trans4, "sample",sampleLog,
            motherVolume, false, 0, true);
  }

  G4Region * fpRegion = new G4Region("Target");
  fpRegion->AddRootLogicalVolume(sampleLog);
  return samplePhy;
}
