//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: DetectorConstruction.cc  $
//
// \file bioex/DetectorConstruction.cc
// implementation of the DetectorConstruction class

#include "DetectorConstruction.hh"
#include "DetectorMessenger.hh"
#include "G4LogicalVolume.hh"
#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "G4Box.hh"
#include "DetectorBuilder.hh"
#include "G4VSolid.hh"

#include "G4tgbVolumeMgr.hh"
#include "G4tgrMessenger.hh"
#include "G4Material.hh"

#include "TestTube.hh"
#include "Eppe05.hh"
#include "Eppe15.hh"
#include "Eppe20.hh"
#include "Eppe_nph.hh"
#include "Flask.hh"
#include "Phantom.hh"

#include "G4PVPlacement.hh"
#include "G4LogicalVolume.hh"
#include "G4SystemOfUnits.hh"
#include "G4NistManager.hh"
#include "G4Transform3D.hh"
#include <sstream>
#include "Randomize.hh"
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
DetectorConstruction::DetectorConstruction()
{
  fMessenger = new G4tgrMessenger;
  fDetMessenger = new DetectorMessenger(this);
  fSampleVolume =  0.4*cm3;
  fSample = NULL;
  fSensitiveCube = NULL;
  fOutputFileName ="output_file.txt";
  fNoOfCubes = 1e3;
  sensCubeTrue = false;
  tubeType = "Eppe15";
  tt = NULL;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
DetectorConstruction::~DetectorConstruction()
{
  delete fMessenger;
  delete fDetMessenger;
  fListOfCubes.clear();
  delete tt;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void DetectorConstruction::SetFileName(G4String fname)
{
    listOfFilenames.push_back(fname);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4VPhysicalVolume* DetectorConstruction::Construct()
{
  //------------------------------------------------
  // Define one or several text files containing the geometry description
  //------------------------------------------------
  G4NistManager* man = G4NistManager::Instance();
  G4Material* Air  = man->FindOrBuildMaterial("G4_AIR");

  G4VSolid*  solidWorld = new G4Box("world", 100.*cm, 100.*cm, 100.*cm);
  G4LogicalVolume*  logicWorld = new G4LogicalVolume(solidWorld,Air,"world");
  G4VPhysicalVolume* physiWorld = new G4PVPlacement(0, G4ThreeVector(),
            "world",logicWorld,0, false, 0);


  ChooseTestTube();
  fTestTube = tt->ConstructTestTube(physiWorld);
  fSample = tt->ConstructSample(physiWorld,fSampleVolume);

  // Setting colors for TestTube and Sample
  G4VisAttributes* sampleVisAtt = new G4VisAttributes(G4Colour(0.,0.,1.0));
  G4VisAttributes* tubeVisAtt = new G4VisAttributes(G4Colour(0.2,0.5,0.));
  sampleVisAtt->SetVisibility(true);
  tubeVisAtt->SetVisibility(true);
  fSample->GetLogicalVolume()->SetVisAttributes(sampleVisAtt);
  fTestTube->GetLogicalVolume()->SetVisAttributes(tubeVisAtt);
  G4VSolid * sampleSolid = fSample->GetLogicalVolume()->GetSolid();
  G4double volsam = sampleSolid->GetCubicVolume();

  cout << "Sample volume is : " << volsam/cm3 << " ml" << endl;

  if (sensCubeTrue) {
    for (G4int j=0; j< fNoOfCubes ; j++ )
     {
       G4double jx,jy,jz;

       do {
           jx = (2*G4UniformRand()-1)*5.0*mm;
           jy = (2*G4UniformRand()-1)*5*mm;
           jz = (2*G4UniformRand()-1)*7.0*cm;

       } while ( sampleSolid->Inside(G4ThreeVector(jx,jy,jz))!= kInside );

       fSensitiveCube = CreateSensitiveCube(256000*nm, G4ThreeVector(jx,jy,jz));
     }
  }

  G4tgbVolumeMgr* volmgr = G4tgbVolumeMgr::GetInstance();
  volmgr->DeleteTextFiles();
  for (auto const & fn: listOfFilenames)
  { volmgr->AddTextFile(fn);  }

  //------------------------------------------------
  // Use your own detector builder, that will invoke your own line processor
  //------------------------------------------------
  DetectorBuilder* gtb = new DetectorBuilder;
  volmgr->SetDetectorBuilder( gtb );

  //const G4tgrVolume* tgrVoltop = gtb->ReadDetector();
  //G4VPhysicalVolume* physiWorld = gtb->ConstructDetector(tgrVoltop);

  return physiWorld;
}

void DetectorConstruction::SetSampleVolume(G4double volume )
{
  fSampleVolume = volume;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void DetectorConstruction::ChooseTestTube()
{
    if (tubeType=="Eppe15" || tubeType=="eppe15" || tubeType=="tube15") {
       tt = new Eppe15();
    } else if (tubeType=="Eppe20" || tubeType=="eppe20" || tubeType=="tube20") {
       tt = new Eppe20();
    } else if (tubeType=="Eppe05" || tubeType=="eppe05" || tubeType=="Eppe5") {
       tt = new Eppe05();
    } else if (tubeType=="Flask" || tubeType=="flask" ) {
       tt = new Flask();
    } else if (tubeType=="Eppe_nph" || tubeType=="eppe_nph" || tubeType=="tube04" ) {
      tt = new Eppe_nph();
    } else if (tubeType=="Phantom" || tubeType=="phantom") {
      tt = new Phantom();
    } else {
       tt = new Eppe15();
       G4cout << "You have chosen none or invalid tube name. " <<
                 "Eppendorf microtube 1.5 ml was chosen." << G4endl;
    }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4VPhysicalVolume*
   DetectorConstruction::CreateSensitiveCube(G4double cubeSide,
                                             G4ThreeVector posit)
{
   if (cubeSide > 1*mm) {G4cout << "Set reasonable size of cube!" << G4endl;}
   G4VSolid * sensCube = new G4Box("sensCube",cubeSide,cubeSide,cubeSide);
   G4NistManager* man = G4NistManager::Instance();
   G4Material* Wasser  = man->FindOrBuildMaterial("G4_WATER");
   G4LogicalVolume* logicCube = new G4LogicalVolume(sensCube,Wasser,"sensCube");
   G4RotationMatrix Ra;
   G4String cubename;
   ostringstream convert;
   int x,y,z;
   x = (int) (posit.getX()/nm);
   convert << x; convert << "_";
   y = (int) (posit.getY()/nm);
   convert << y; convert << "_";
   z = (int) (posit.getZ()/nm);
   convert << z;
   cubename = (G4String) convert.str();
   //std::cout << "CUBE NAME " << cubename << std::endl;
   G4Transform3D t = G4Transform3D(Ra,posit);
   G4VPhysicalVolume* physCube = new G4PVPlacement(t,
         cubename,logicCube,fSample,false,0,true);
   fListOfCubes.insert(cubename);
   return physCube;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4VPhysicalVolume* DetectorConstruction::GetSensitiveCube()
{
   return fSensitiveCube;
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
