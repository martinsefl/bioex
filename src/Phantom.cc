#include "Phantom.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4Region.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4VSolid.hh"

#include "G4RotationMatrix.hh"
#include "G4UnionSolid.hh"
#include "G4NistManager.hh"
#include "G4Material.hh"
#include "G4PhysicalConstants.hh"
#include "G4Transform3D.hh"
#include "G4PVPlacement.hh"
#include "G4Box.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
Phantom::Phantom() : TestTube()
{
    fFirstLayerThickness = 0.8*mm;
    fWaterThickness = 20*cm;
    RU0 = G4RotationMatrix(); RU0.rotateY(pi);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
Phantom::~Phantom()
{ ; }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4VPhysicalVolume* Phantom::ConstructTestTube(G4VPhysicalVolume* motherVolume)
{
   G4NistManager* man = G4NistManager::Instance();
   man->SetVerbose(1);
   G4Material* polyp = man->FindOrBuildMaterial("G4_POLYPROPYLENE");

   G4VSolid * firstLayer = new G4Box("Plastic",10.*cm,10.*cm,fFirstLayerThickness/2.);
   G4LogicalVolume * firstLayerlogic = new G4LogicalVolume(firstLayer,
              polyp,"Plastic", 0,0,0,true);
   G4Transform3D place1 (R01,G4ThreeVector(0,0,-fFirstLayerThickness/2.));
   G4VPhysicalVolume* plastic = new G4PVPlacement(place1,"Plastic",
              firstLayerlogic, motherVolume, false, 0, true);
   return plastic;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4VPhysicalVolume* Phantom::ConstructSample(
  G4VPhysicalVolume* motherVolume, G4double volumeOfSample = 1*cm3)
{
  G4NistManager* man = G4NistManager::Instance();
  man->SetVerbose(1);
  G4Material* sample_mat = man->FindOrBuildMaterial("G4_WATER");
  G4VSolid * waterSolid = new G4Box("Water",10.*cm,10.*cm,fWaterThickness/2.);
  G4LogicalVolume * waterLog = new G4LogicalVolume(waterSolid,sample_mat,"Water",
     0,0,0,true);
  G4Transform3D place1 (R01,G4ThreeVector(0,0,fWaterThickness/2.));
  G4VPhysicalVolume* waterPhy = new G4PVPlacement(place1,"Water",
            waterLog, motherVolume, false, 0, true);

  G4Region * fpRegion = new G4Region("Target");
  fpRegion->AddRootLogicalVolume(waterLog);
  return waterPhy;
}
