//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file bioex/src/Run.cc
/// \brief Implementation of the Run class
//
// $Id: Run.cc  $
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//
//
//  (Description)
//    Run Class is for accumulating scored quantities which is
//  scored using G4MutiFunctionalDetector and G4VPrimitiveScorer.
//  Accumulation is done using G4THitsMap object.
//
//    The constructor Run(const std::vector<G4String> mfdName)
//  needs a vector filled with MultiFunctionalDetector names which
//  was assigned at instantiation of MultiFunctionalDetector(MFD).
//  Then Run constructor automatically scans primitive scorers
//  in the MFD, and obtains collectionIDs of all collections associated
//  to those primitive scorers. Futhermore, the G4THitsMap objects
//  for accumulating during a RUN are automatically created too.
//  (*) Collection Name is same as primitive scorer name.
//
//    The resultant information is kept inside Run objects as
//  data members.
//  std::vector<G4String> fCollName;            // Collection Name,
//  std::vector<G4int> fCollID;                 // Collection ID,
//  std::vector<G4THitsMap<G4double>*> fRunMap; // HitsMap for RUN.
//
//  The resualtant HitsMap objects are obtain using access method,
//  GetHitsMap(..).
//
//=====================================================================

#include "Run.hh"
#include "DetectorConstruction.hh"
#include "G4Event.hh"
#include "G4SDManager.hh"

#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4String.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include <fstream>
#include <ostream>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

Run::Run(const DetectorConstruction* detector,const std::vector<G4String> mfdName)
: G4Run(),
  fDetector(detector),
  fEdeposit(0.),  fEdeposit2(0.),fNoOfEvents(0),fEventDep(0.),fEventDep2(0.)
{
  G4SDManager* pSDman = G4SDManager::GetSDMpointer();
  //=================================================
  //  Initalize RunMaps for accumulation.
  //  Get CollectionIDs for HitCollections.
  //=================================================
  G4int nMfd = mfdName.size();
  for ( G4int idet = 0; idet < nMfd ; idet++){  // Loop for all MFD.
    G4String detName = mfdName[idet];
    //--- Seek and Obtain MFD objects from SDmanager.
    G4MultiFunctionalDetector* mfd =
      (G4MultiFunctionalDetector*)(pSDman->FindSensitiveDetector(detName));
    //
    if ( mfd ){
      //--- Loop over the registered primitive scorers.
      for (G4int icol = 0; icol < mfd->GetNumberOfPrimitives(); icol++){
        // Get Primitive Scorer object.
        G4VPrimitiveScorer* scorer=mfd->GetPrimitive(icol);
        // collection name and collectionID for HitsCollection,
        // where type of HitsCollection is G4THitsMap in case of primitive
        // scorer.
        // The collection name is given by <MFD name>/<Primitive Scorer name>.
        G4String collectionName = scorer->GetName();
        G4String fullCollectionName = detName+"/"+collectionName;
        G4int    collectionID = pSDman->GetCollectionID(fullCollectionName);
        //
        if ( collectionID >= 0 ){
          G4cout << "++ "<<fullCollectionName<< " id " << collectionID
                 << G4endl;
          // Store obtained HitsCollection information into data members.
          // And, creates new G4THitsMap for accumulating quantities during RUN.
          fCollName.push_back(fullCollectionName);
          fCollID.push_back(collectionID);
          fRunMap.push_back(new G4THitsMap<G4double>(detName,collectionName));
        } else {
          G4cout << "** collection " << fullCollectionName << " not found. "
                 << G4endl;
        }
      }
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

Run::~Run()
{
  //--- Clear HitsMap for RUN
  G4int nMap = fRunMap.size();
  for ( G4int i = 0; i < nMap; i++){
    if(fRunMap[i] ) fRunMap[i]->clear();
  }
  fCollName.clear();
  fCollID.clear();
  fRunMap.clear();
  //fCubesDeposits.clear();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void Run::AddEdep (G4double e)
{
  fEdeposit  += e;
//  fEdeposit2 += e*e;
  fEventDep += e;
  fEventDep2+=e*e;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void Run::Merge(const G4Run * aRun) {
  const Run * localRun = static_cast<const Run *>(aRun);

  //=======================================================
  // Merge HitsMap of working threads
  //=======================================================
  G4int nCol = localRun->fCollID.size();
  for ( G4int i = 0; i < nCol ; i++ ){  // Loop over HitsCollection
    if ( localRun->fCollID[i] >= 0 ){
      *fRunMap[i] += *localRun->fRunMap[i];
    }
  }
  fEdeposit+=localRun->fEdeposit;
  fEventDep2+=localRun->fEventDep2;
  fEdeposit2+=localRun->fEdeposit2;
  fNoOfEvents+=localRun->fNoOfEvents;


  for (auto it=localRun->fCubesDeposits.begin(); it!=localRun->fCubesDeposits.end(); ++it) {
  if ( fCubesDeposits[it->first] )
    fCubesDeposits[it->first] += it->second;
  else
    fCubesDeposits[it->first] = it->second;
}
  //fCubesDeposits+=localRun->fCubesDeposits;
  // need to merge two maps!!
  G4Run::Merge(aRun);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void Run::EndOfRun()
{


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void Run::AddCubeDeposit(G4String cube_id, G4double dep)
{

  if ( fCubesDeposits.count(cube_id) ==0 ) {
    fCubesDeposits[cube_id]=dep;
    // not found
  } else {
    // found
  fCubesDeposits[cube_id]+=dep;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void Run::PrintDeposits()
{

    G4double stddev = std::sqrt(fEdeposit2 - fEdeposit*fEdeposit/(G4double)numberOfEvent);
    G4double mass = fDetector->GetSampleVolume()->GetLogicalVolume()->GetMass();
    G4cout << " Deposits in sample was " << fEdeposit/MeV << " MeV, std dev : " << stddev/(MeV)/numberOfEvent << " MeV" << G4endl;
    G4cout << " Mass of sample was " << mass/kg <<  " kg." << G4endl;
    G4cout << " Mean dose was " << fEdeposit/mass/gray <<  " Gy " << G4endl;
    G4cout << " Number of events " << numberOfEvent  << " fNoOfEvents" << fNoOfEvents;
}

void Run::PrintCubeDeposits(){
std::ofstream  file("cube_"+fDetector->GetOutputFileName());
file <<  " deposits in eV in random cubes" << endl;
// print it to the file
  for (auto it=fCubesDeposits.begin(); it!=fCubesDeposits.end(); ++it) {
   file << it->first << " " << it->second/eV << endl;
 }
file.close();

}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//
//  RecordEvent is called at end of event.
//  For scoring purpose, the resultant quantity in a event,
//  is accumulated during a Run.
void Run::RecordEvent(const G4Event* aEvent)
{
  numberOfEvent++;  // This is an original line.
  fNoOfEvents++;
  fEdeposit2+=fEventDep*fEventDep;
  fEventDep = 0.;


  //=============================
  // HitsCollection of This Event
  //============================
  G4HCofThisEvent* pHCE = aEvent->GetHCofThisEvent();
  if (!pHCE) return;

  //=======================================================
  // Sum up HitsMap of this Event  into HitsMap of this RUN
  //=======================================================
  G4int nCol = fCollID.size();
  for ( G4int i = 0; i < nCol ; i++ ){  // Loop over HitsCollection
    G4THitsMap<G4double>* evtMap=0;
    if ( fCollID[i] >= 0 ){           // Collection is attached to pHCE
      evtMap = (G4THitsMap<G4double>*)(pHCE->GetHC(fCollID[i]));
    }else{
      G4cout <<" Error evtMap Not Found "<< i << G4endl;
    }
    if ( evtMap )  {
      //=== Sum up HitsMap of this event to HitsMap of RUN.===
      *fRunMap[i] += *evtMap;
      //======================================================
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//
//  Access method for HitsMap of the RUN
//
//-----
// Access HitsMap.
//  By  MultiFunctionalDetector name and Collection Name.
G4THitsMap<G4double>* Run::GetHitsMap(const G4String& detName,
                                         const G4String& colName)
{
    G4String fullName = detName+"/"+colName;
    return GetHitsMap(fullName);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//
//-----
// Access HitsMap.
//  By full description of collection name, that is
//    <MultiFunctional Detector Name>/<Primitive Scorer Name>
G4THitsMap<G4double>* Run::GetHitsMap(const G4String& fullName){
    G4int nCol = fCollName.size();
    for ( G4int i = 0; i < nCol; i++){
        if ( fCollName[i] == fullName ){
            return fRunMap[i];
        }
    }
    return NULL;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//
//-----
// - Dump All HitsMap of this RUN. (for debuging and monitoring of quantity).
//   This method calls G4THisMap::PrintAll() for individual HitsMap.
void Run::DumpAllScorer(){

  // - Number of HitsMap in this RUN.
  G4int n = GetNumberOfHitsMap();
  // - GetHitsMap and dump values.
  for ( G4int i = 0; i < n ; i++ ){
    G4THitsMap<G4double>* runMap =GetHitsMap(i);
    if ( runMap ) {
      G4cout << " PrimitiveScorer "
             << runMap->GetSDname() <<","<< runMap->GetName() << G4endl;
      G4cout << " Number of entries " << runMap->entries() << G4endl;
      std::map<G4int,G4double*>::iterator itr = runMap->GetMap()->begin();
      for(; itr != runMap->GetMap()->end(); itr++) {
        G4cout << "  copy no.: " << itr->first
               << "  Run Value : " << *(itr->second)
               << G4endl;
      }
    }
  }
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
