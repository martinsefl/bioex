# BioEx example

The BioEx example is a Geant4-based example which can help you with simulations of experiments, 
in which liquid biological samples are irradiated in microtubes.

## Prerequisites
The application is provided in the form of source code. To compile and use it, 
a computer system with a Geant4 installation is needed. The code has been tested with version 10.3 on a GNU/Linux system.

Detailed instructions for native installation can be found at the [Geant4 website](http://geant4.cern.ch/support/gettingstarted.shtml).

Alternatively, you can also use the Geant4 virtual machine:

1. Download a Geant4 virtual machine [http://geant4.in2p3.fr/spip.php?rubrique8](http://geant4.in2p3.fr/spip.php?rubrique8). 
1. Download and install a software to run the virtual machine (VMware Player, Parallels Desktop, etc.)
1. Open the image in the virtualization software and open the Terminal 

## Installation

Open terminal. Go to the folder, where you want to download and build your example. 
(Let us say it will be in ~/Desktop/). Type the following commands into the terminal.

The lines starting with \# are comments.

```
cd ~/Desktop/
git clone git@bitbucket.org:martinsefl/bioex.git
mkdir build 
cd build
# Run CMAKE
# $G4COMP is the path to Geant4 installation - this works on virtual machine
cmake -DGeant4_DIR=$G4COMP ../bioex
# build the application
make
# run the application with the inputfile.mac
./bioex inputfile.mac
```

The inputfile.mac can look like this:

```
# The choice of microtube: 
# Eppe20: 2.0 ml microtube
# Eppe15: 1.5 ml microtube
# Eppe04: 0.4 ml microtube
/bioex/testTube Eppe20
# The coordinates of the center of the voxel grid.
/bioex/voxels/positionOfGrid 0 -6 0 mm
# The size of the voxel cube.
/bioex/voxels/voxelSize 0.1 mm
# The numbers of voxel in the x, y, z direction. 
/bioex/voxels/numOfVoxelsXYZ 110 350 110
# so the voxel grid will be between -5.5 mm and  5.5 mm in x
#                                  -23.5 mm and 11.5 mm in y 
#                                   -5.5 mm and  5.5 mm in z
# the setting of the sample volume (the water in the tube)
/bioex/det/sampleVolume 0.01 cm3 
# set the outputfile name, in which the dose in voxel grid will be written
/bioex/det/outputFileName outputFile.txt

# GENERAL PARTICLE SOURCE SETTINGS 
/gps/verbose 0
/gps/direction 0. 0. -1.
/gps/particle proton
/gps/pos/type Plane
/gps/pos/shape Rectangle
/gps/pos/centre 0 -6 10 mm
/gps/pos/halfx 6 mm
/gps/pos/halfy 18 mm
/gps/ene/type Mono
/gps/ene/mono 20 MeV
/run/initialize 
# 1000 primary particles 
/run/beamOn 1000
```

Details on the general particle source can be found in the [Geant4 Guide for Application Developer](
http://geant4.web.cern.ch/geant4/UserDocumentation/UsersGuides/ForApplicationDeveloper/html/GettingStarted/generalParticleSource.html).

The mean dose in the sample will be written to standard output, the dose in voxels will be written into outputFile.txt.

### Contact

If you have any comments or suggestions, we'll be glad to hear them.

Martin Šefl, [sefl@ujf.cas.cz](mailto://sefl@ujf.cas.cz)

Václav Štěpán, [stepan@ujf.cas.cz](mailto://stepan@ujf.cas.cz)

Kateřina Pachnerová Brabcová, [brabcova@ujf.cas.cz](mailto://brabcova@ujf.cas.cz)
