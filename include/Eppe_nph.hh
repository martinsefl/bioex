/// \file bioex/include/Eppe_nph.hh
/// \brief Definition of the Eppe_nph class
///  Eppendorf Microtube Tube

#ifndef Eppe_nph_HH
#define Eppe_nph_HH

#include "globals.hh"
#include "G4RotationMatrix.hh"

#include "TestTube.hh"

class G4VPhysicalVolume ;

class Eppe_nph : public TestTube
{
  public:

    Eppe_nph();
    virtual ~Eppe_nph();

    virtual G4VPhysicalVolume* ConstructTestTube(G4VPhysicalVolume* motherVolume);
    virtual G4VPhysicalVolume* ConstructSample(G4VPhysicalVolume* motherVolume, G4double volumeOfSample);

  private:
    // helper rot. matrices
    G4RotationMatrix R00,R01,RU0;
    // dimensions
    G4double fInnerUpperRad,fInnerMidRad,fInnerTipRad;
    G4double fOuterUpperRad, fOuterMidRad, fOuterTipRad;
    G4double fBottomHeight,fUpperHeight;
};

#endif
