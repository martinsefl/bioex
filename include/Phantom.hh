/// \file bioex/include/Phantom.hh
/// \brief Definition of the phantom class
/// water phantom with plastic window
#ifndef Phantom_HH
#define Phantom_HH
#include "globals.hh"
#include "G4RotationMatrix.hh"

#include "TestTube.hh"

class G4VPhysicalVolume;
class G4VSolid;

class Phantom : public TestTube
{
  public:

    Phantom();
    virtual ~Phantom();

    virtual G4VPhysicalVolume* ConstructTestTube(G4VPhysicalVolume* motherVolume);
    virtual G4VPhysicalVolume* ConstructSample(G4VPhysicalVolume* motherVolume, G4double volumeOfSample);

  private:
    // helper rot. matrices
    G4RotationMatrix R00,R01,RU0;
    // dimensionsr
    G4double fFirstLayerThickness;
    G4double fWaterThickness;
};

#endif
