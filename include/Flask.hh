/// \file bioex/include/Flask.hh
/// \brief Definition of the Flask class
#ifndef Flask_HH
#define Flask_HH

#include "globals.hh"
#include "G4RotationMatrix.hh"
#include "TestTube.hh"
#include "G4TwoVector.hh"

/// TPP 25 cm2 flask
class G4VPhysicalVolume;

class Flask : public TestTube
{
  public:

    Flask();
    virtual ~Flask();

    virtual G4VPhysicalVolume* ConstructTestTube(G4VPhysicalVolume* motherVolume);
    virtual G4VPhysicalVolume* ConstructSample(G4VPhysicalVolume* motherVolume, G4double volumeOfSample);

  private:
    G4RotationMatrix R00,R01,RU0;
    G4double hW0,hL0,hNh0, hNw0,wW;
    G4double hH;
    std::vector <G4TwoVector> pointsOut, pointsIn;
};

#endif
