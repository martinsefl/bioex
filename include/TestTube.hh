//
/// \file bioex/include/TestTube.hh
/// \brief Definition of the TestTube class

#ifndef TestTube_HH
#define TestTube_HH

#include "G4VPhysicalVolume.hh"

/// this is just base class for test tubes,
/// provides help methods to calculate half sphere(cone) height for a given volume

class TestTube
{
  public:
    TestTube();
    virtual ~TestTube();

    virtual G4VPhysicalVolume*
            ConstructTestTube(G4VPhysicalVolume* ) = 0;
    virtual G4VPhysicalVolume*
            ConstructSample(G4VPhysicalVolume* , G4double volumeOfSample) = 0;

    G4double GetHalfEllipsoidHeight(G4double volume,G4double aX,
      G4double aY, G4double aZ);
    G4double GetConeHeight(G4double, G4double, G4double, G4double);
    G4double GetSphereHeight(G4double volume, G4double radius);
    G4double GetRadiusOfConePart(G4double, G4double, G4double, G4double);
};

#endif
