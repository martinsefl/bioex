//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file bioex//include/PSDoseDeposit3D.hh
/// \brief Definition of the PSDoseDeposit3D class
//
// $Id: PSDoseDeposit3D.hh 66501 2012-12-19 09:25:23Z gcosmo $
//

#ifndef PSDoseDeposit3D_h
#define PSDoseDeposit3D_h 1

#include "G4PSDoseDeposit3D.hh"
///////////////////////////////////////////////////////////////////////////////
// (Description)
//   This is a primitive scorer class for scoring energy deposit.
//
///////////////////////////////////////////////////////////////////////////////

class PSDoseDeposit3D : public G4PSDoseDeposit3D
{
   public: // with description
      PSDoseDeposit3D(G4String name,G4int nx,G4int ny, G4int nz);
      virtual ~PSDoseDeposit3D();

  protected: // with description
      virtual G4int GetIndex(G4Step*);

  private:
      G4int fNx, fNy, fNz;
};
#endif
