//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file bioex/include/Run.hh
/// \brief Definition of the Run class
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef Run_h
#define Run_h 1

#include "DetectorConstruction.hh"
#include "G4Run.hh"
#include "G4THitsMap.hh"
#include <vector>

class G4Event;
class DetectorConstruction;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
class Run : public G4Run
{
  public:
    Run(const DetectorConstruction* detector,const std::vector<G4String> mfdName);
   ~Run();

  public:
    void AddEdep (G4double e);
    G4double GetEdep() const {return fEdeposit;};

    virtual void Merge(const G4Run*);
    void EndOfRun();
    void RecordEvent(const G4Event*);

    // Access methods for scoring information.
    // - Number of HitsMap for this RUN.
    //   This is equal to number of collections.
    G4int GetNumberOfHitsMap() const {return fRunMap.size();}
    // - Get HitsMap of this RUN.
    //   by sequential number, by multifucntional name and collection name,
    //   and by collection name with full path.
    G4THitsMap<G4double>* GetHitsMap(G4int i){return fRunMap[i];}
    G4THitsMap<G4double>* GetHitsMap(const G4String& detName,
                                  const G4String& colName);
    G4THitsMap<G4double>* GetHitsMap(const G4String& fullName);
    // - Dump All HitsMap of this RUN.
    //   This method calls G4THisMap::PrintAll() for individual HitsMap.
    void DumpAllScorer();
    void PrintDeposits();

    void AddCubeDeposit(G4String,G4double);
    void PrintCubeDeposits();
  private:
    const DetectorConstruction*  fDetector;
    G4double   fEdeposit,    fEdeposit2;
    G4int fNoOfEvents;
    G4double fEventDep, fEventDep2;

    std::vector<G4String> fCollName;
    std::vector<G4int> fCollID;
    std::vector<G4THitsMap<G4double>*> fRunMap;
    // storing mini-cubes deposits
    std::map<G4String,G4double> fCubesDeposits;
};
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#endif
