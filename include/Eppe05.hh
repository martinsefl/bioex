// $Id: Eppe05.hh  $
/// \file bioex/include/Eppe05.hh
/// \brief Definition of the Eppe05 class
/// 0.5 ml Eppendorf TestTube
#ifndef Eppe05_HH
#define Eppe05_HH

#include "globals.hh"
#include "G4RotationMatrix.hh"
#include "TestTube.hh"

class G4VPhysicalVolume;

class Eppe05 : public TestTube
{
  public:

    Eppe05();
    virtual ~Eppe05();

    virtual G4VPhysicalVolume* ConstructTestTube(G4VPhysicalVolume* motherVolume);
    virtual G4VPhysicalVolume* ConstructSample(G4VPhysicalVolume* motherVolume, G4double volumeOfSample);

  private:
    // helper rot. matrices
    G4RotationMatrix R00,R01,RU0;
    // dimensions
    G4double fInnerUpperRad,fInnerMidRad,fInnerTipRad;
    G4double fOuterUpperRad, fOuterMidRad, fOuterTipRad;
    G4double fBottomHeight,fUpperHeight;
};

#endif
