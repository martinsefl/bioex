//
// $Id: ParallelMessenger.hh -- Sefl, M. 2nd May 2017
//
/// \file bioex/include/DetectorMessenger.hh
/// \brief Definition of the DetectorMessenger class

#ifndef DetectorMessenger_h
#define DetectorMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class DetectorConstruction;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithoutParameter;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class DetectorMessenger: public G4UImessenger
{
     public:

        DetectorMessenger(DetectorConstruction* );
        ~DetectorMessenger();

        virtual void SetNewValue(G4UIcommand*, G4String);

      private:
         DetectorConstruction*      fDetector;

         G4UIdirectory*             fBioExDir;
         G4UIdirectory*             fDetDir;
         //G4UIdirectory*             fTestTubeDir;
         G4UIcmdWithAString*        fFileNameCmd;
         G4UIcmdWithAString*        fTestTubeChoice;
	       G4UIcmdWithoutParameter*   fConstructCmd;
	       G4UIcmdWithADoubleAndUnit* fSampleVolumeCmd;

         G4UIcmdWithAString*        fOutputFileNameCmd;

  };

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
