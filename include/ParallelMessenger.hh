//
// $Id: ParallelMessenger.hh -- Sefl, M. 2nd May 2017
//
/// \file bioex/include/ParallelMessenger.hh
/// \brief Definition of the ParallelMessenger class

#ifndef ParallelMessenger_h
#define ParallelMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class ParallelWorld;
class G4UIdirectory;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3Vector;
class G4UIcmdWith3VectorAndUnit;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class ParallelMessenger: public G4UImessenger
{
     public:

        ParallelMessenger(ParallelWorld* );
        ~ParallelMessenger();

        virtual void SetNewValue(G4UIcommand*, G4String);

      private:

         ParallelWorld*             fParallel;

         G4UIdirectory*              fVoxDir;
	 G4UIcmdWithADoubleAndUnit*        fVoxelSize;
	 G4UIcmdWith3Vector*               fNumberOfVoxels;
   G4UIcmdWith3VectorAndUnit*        fPositionOfGrid;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
