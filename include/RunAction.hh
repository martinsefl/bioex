//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file bioex/include/RunAction.hh
/// \brief Definition of the RunAction class
//
// $Id: RunAction.hh  $

#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"

#include <vector>

#include "G4String.hh"

class Run;
class DetectorConstruction;
class SteppingAction;


/// For Particle info storage
//
struct particleInfo {
  G4ThreeVector  momentumDir;
  G4ThreeVector  position;
  G4String particleName;
  G4double energy;
  G4double time;
};


/// RunAction Class
class RunAction : public G4UserRunAction
{
  public:
    RunAction();
    virtual ~RunAction();

  public:

    virtual void BeginOfRunAction(const G4Run*);
    virtual void EndOfRunAction(const G4Run*);
    virtual G4Run* GenerateRun();

    void PrintParticleInfo(G4String);

    inline void StoreParticleInfo(particleInfo part)
                { fParticleInfo.push_back(part); }


    // Utility method for converting segment number of
    // water phantom to copyNo of HitsMap.
    G4int CopyNo(G4int ix, G4int iy, G4int iz)
         {  return (iy*(fNx*fNz)+ix*fNz+iz); }



  private:
    // Data member
    // - vector of MultiFunctionalDetecor names.
    std::vector<G4String> fSDName;

    Run* fpRun;
    const DetectorConstruction* fpDetector;
    const SteppingAction* fSteppingAction ;

    // for conversion of sengment number to copyNo.
    G4int fNx, fNy, fNz;
    // storage of information about particles
    std::vector<particleInfo> fParticleInfo;

};

#endif
