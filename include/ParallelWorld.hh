/// \file bioex/include/ParallelWorld.hh
/// \brief Definition of the ParallelWorld class
//
// $Id: ParallelWorld.hh  $
/// Parallel geometry for voxel scoring

#ifndef ParallelWorld_h
#define ParallelWorld_h 1

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4VUserParallelWorld.hh"

class ParallelMessenger;
class G4LogicalVolume;

class ParallelWorld : public G4VUserParallelWorld
{
  public:
    ParallelWorld(G4String worldName);
    virtual ~ParallelWorld();

  public:
    virtual void Construct();
    virtual void ConstructSD();
    void SetupDetectors();

    void SetNumberOfVoxels(G4ThreeVector );

    inline void SetVoxelSize(G4double vS) {voxSize = vS;}

    void GetNumberOfSegmentsInPhantom(G4int& nx, G4int& ny, G4int& nz)
     const{ nx=fNX; ny = fNY; nz = fNZ; }

    void SetPositionOfGrid(G4ThreeVector);

    inline G4double GetVoxelSize() { return voxSize; }
  private:
    ParallelMessenger* fParallelMessenger;

    G4double voxSize;
    G4int fNX,fNY,fNZ;
    G4double fPX,fPY,fPZ;
    G4LogicalVolume* ghLogical;
    G4LogicalVolume* yRepLog;
    G4LogicalVolume* xRepLog;
    G4LogicalVolume* zRepLog;

};

#endif
