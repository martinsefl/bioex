//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file bioex/include/DetectorConstruction.hh
/// \brief Definition of the DetectorConstruction class
//
// $Id: DetectorConstruction.hh  $

#ifndef DetectorConstruction_HH
#define DetectorConstruction_HH
#include <set>
#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4VisAttributes.hh"

class G4tgrMessenger;
class DetectorMessenger;
class Eppe15;
class TestTube;
using namespace std;

/// Detector construction class using text geometry file using cuts per region

class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:

    DetectorConstruction();
   ~DetectorConstruction();

    G4VPhysicalVolume* Construct();
    void SetFileName(G4String);
    void ConstructSensitiveVolume();
    void SetSampleVolume(G4double volume);
    void ChooseTestTube();

    G4bool IsSensCube() { return sensCubeTrue;}
    G4bool IsSensCube() const { return sensCubeTrue;}

    // creates cube inside fSample
    G4VPhysicalVolume* CreateSensitiveCube(G4double,G4ThreeVector);
    G4VPhysicalVolume* GetSensitiveCube();

    inline void SetTestTube(G4String typ) { tubeType=typ; }
    inline  G4VPhysicalVolume* GetSampleVolume() { return fSample; }
    inline  G4VPhysicalVolume* GetSampleVolume() const { return fSample; }

    inline G4String GetOutputFileName() const { return fOutputFileName; }
    inline void SetOutputFileName(G4String opfn) { fOutputFileName = opfn; }

   private:
    G4bool sensCubeTrue;
    G4tgrMessenger* fMessenger;
    DetectorMessenger*  fDetMessenger;
    G4VPhysicalVolume* fSample;
    G4VPhysicalVolume* fTestTube;
    G4VPhysicalVolume* fSensitiveCube;
    TestTube * tt;

    G4String tubeType;
    G4double fSampleVolume;
    std::vector<G4String> listOfFilenames;
    std::set<G4String> fListOfCubes;
    G4int fNoOfCubes;
    G4String       fOutputFileName;
};

#endif
