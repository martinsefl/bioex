/// \file bioex/include/Eppe20.hh
/// \brief Definition of the Eppe20 class
/// 2.0 ml Eppendorf TestTube
#ifndef Eppe20_HH
#define Eppe20_HH 
#include "globals.hh"
#include "G4RotationMatrix.hh"

#include "TestTube.hh"

//2.0  ml Eppendorf TestTube

// consist of cut cone and halfsphere

/// Detector construction class using text geometry file using cuts per region
class G4VPhysicalVolume;
class G4VSolid;

class Eppe20 : public TestTube
{
  public:

    Eppe20();
    virtual ~Eppe20();

    virtual G4VPhysicalVolume* ConstructTestTube(G4VPhysicalVolume* motherVolume);
    virtual G4VPhysicalVolume* ConstructSample(G4VPhysicalVolume* motherVolume, G4double volumeOfSample);

  private:
    // helper rot. matrices
    G4RotationMatrix R00,R01,RU0;
    // dimensionsr
    G4double IR1,IR2,OR1,OR2, CH1,IES,OES;
    G4VSolid* eppe20in;
};

#endif
