/// \file bioex/include/Eppe15.hh
/// \brief Definition of the Eppe15 class
/// 1.5 ml Eppendorf TestTube

#ifndef Eppe15_HH
#define Eppe15_HH

#include "globals.hh"
#include "G4RotationMatrix.hh"

#include "TestTube.hh"

class G4VPhysicalVolume ;

class Eppe15 : public TestTube
{
  public:

    Eppe15();
    virtual ~Eppe15();

    virtual G4VPhysicalVolume* ConstructTestTube(G4VPhysicalVolume* motherVolume);
    virtual G4VPhysicalVolume* ConstructSample(G4VPhysicalVolume* motherVolume, G4double volumeOfSample);

  private:
    // helper rot. matrices
    G4RotationMatrix R00,R01,RU0;
    // dimensions
    G4double fInnerUpperRad,fInnerMidRad,fInnerTipRad;
    G4double fOuterUpperRad, fOuterMidRad, fOuterTipRad;
    G4double fBottomHeight,fUpperHeight;
};

#endif
